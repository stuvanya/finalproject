import arcade
import random

SPRITE_SCALING = 0.5

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 650
SCREEN_TITLE = "Knight and Dragon"
EMERALD_COUNT = 20

MOVEMENT_SPEED = 5
SPRITE_SPEED = 1

TEXTURE_LEFT = 0
TEXTURE_RIGHT = 1

class Unit(arcade.Sprite):
    def __init__(self, sprite, scaling, isPlayer):
        super().__init__()
        self.scale = scaling

        self.textures = []
        texture = arcade.load_texture(sprite, flipped_horizontally=True)
        self.textures.append(texture)
        texture = arcade.load_texture(sprite)
        self.textures.append(texture)
        self.set_texture(TEXTURE_RIGHT)

        self.score = 0
        self.isPlayer = isPlayer

    def update(self):
        if(self.change_x > 0):
            self.texture = self.textures[TEXTURE_RIGHT]
        elif(self.change_x < 0):
            self.texture = self.textures[TEXTURE_LEFT]

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.left < 0:
            self.left = 0
        elif self.right > SCREEN_WIDTH - 1:
            self.right = SCREEN_WIDTH - 1

        if self.bottom < 0:
            self.bottom = 0
        elif self.top > SCREEN_HEIGHT - 1:
            self.top = SCREEN_HEIGHT - 1

    def follow_sprite(self, player_sprite):
        if self.center_y < player_sprite.center_y:
            self.change_y = min(SPRITE_SPEED, player_sprite.center_y - self.center_y)
        elif self.center_y > player_sprite.center_y:
            self.change_y = -1 * min(SPRITE_SPEED, self.center_y - player_sprite.center_y)
        else:
            self.change_y = 0

        if self.center_x < player_sprite.center_x:
            self.change_x = min(SPRITE_SPEED, player_sprite.center_x - self.center_x)
        elif self.center_x > player_sprite.center_x:
            self.change_x = -1 * min(SPRITE_SPEED, self.center_x - player_sprite.center_x)
        else:
            self.change_x = 0

class Emerald(arcade.Sprite):
    def update(self):
        self.angle += self.change_angle

class MyGame(arcade.Window):
    def __init__(self, width, height, title):
        super().__init__(width, height, title)

        self.coin_collect_sound = arcade.load_sound("sound/coin_collect.wav")

        self.player_list = None
        self.dragon_list = None
        self.coin_list = None

        self.player_sprite = None
        self.dragon_sprite = None

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        self.player_list = arcade.SpriteList()
        self.dragon_list = arcade.SpriteList()
        self.coin_list = arcade.SpriteList()

        self.player_sprite = Unit("img/knight.png", SPRITE_SCALING, True)
        self.player_sprite.center_x = 50
        self.player_sprite.center_y = 50
        self.player_list.append(self.player_sprite)


        self.dragon_sprite = Unit("img/dragon.png", SPRITE_SCALING, False)
        self.dragon_sprite.center_x = 550
        self.dragon_sprite.center_y = 550
        self.dragon_list.append(self.dragon_sprite)

        self.in_game = True


        for i in range(EMERALD_COUNT):
            coin = arcade.Sprite("img/emerald.png",
                                 SPRITE_SCALING)

            coin.center_x = random.randrange(SCREEN_WIDTH)
            coin.center_y = random.randrange(SCREEN_HEIGHT)
            coin.angle = random.randrange(360)
            coin.change_angle = random.randrange(-5, 6)

            self.coin_list.append(coin)

    def on_draw(self):
        arcade.start_render()

        self.player_list.draw()
        self.dragon_list.draw()
        self.coin_list.draw()
        
        output = f"Player score: {self.player_sprite.score}"
        arcade.draw_text(output, 10, 20, arcade.color.WHITE, 14, anchor_x="left")
        output2 = f"Dragon score: {self.dragon_sprite.score}"
        arcade.draw_text(output2, 590, 20, arcade.color.WHITE, 14, anchor_x="right")

        if (not self.in_game):
            output3 = ""
            if (self.dragon_sprite.score > self.player_sprite.score):
                output3 = "You lose!\nPress any key for new game."
            elif (self.dragon_sprite.score < self.player_sprite.score):
                output3 = "You win!\nPress any key for new game."
            else:
                output3 = "Draw!\nPress any key for new game."
            arcade.draw_text(output3, 300, 350, arcade.color.WHITE, 24, align="center", anchor_x="center", anchor_y="center")

    def on_update(self, delta_time):
        if (not self.in_game):
            return

        self.player_list.update()
        self.dragon_list.update()
        self.coin_list.update()

        player_coins_hit_list = arcade.check_for_collision_with_list(self.player_sprite, self.coin_list)
        dragon_coins_hit_list = arcade.check_for_collision_with_list(self.dragon_sprite, self.coin_list)

        for coin in player_coins_hit_list:
            self.coin_collect_sound.play()
            coin.remove_from_sprite_lists()
            self.player_sprite.score += 1
        for coin in dragon_coins_hit_list:
            self.coin_collect_sound.play()
            coin.remove_from_sprite_lists()
            self.dragon_sprite.score += 1

        self.dragon_sprite.follow_sprite(self.player_sprite)

        player_dragon_hit_list = arcade.check_for_collision_with_list(self.dragon_sprite, self.player_list)
        if (len(player_dragon_hit_list) != 0):
            self.in_game = False

    def on_key_press(self, key, modifiers):
        if (not self.in_game):
            self.setup()
            return

        if key == arcade.key.UP:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT:
            self.player_sprite.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.player_sprite.change_x = MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        if (not self.in_game):
            return

        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.player_sprite.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.player_sprite.change_x = 0

def main():
    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()